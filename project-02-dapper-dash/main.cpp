#include <raylib.h>

struct AnimData
{
    Rectangle rec;
    Vector2 pos;
    int frame;
    float update_time;
    float running_time;
};

bool isOnGround(AnimData data, int win_height)
{
    return data.pos.y >= win_height - data.rec.height;
}

AnimData updateAnimData(AnimData data, float delta_time, int maxFrame)
{
    // Update running time
    data.running_time += delta_time;

    if (data.running_time >= data.update_time)
    {
        data.running_time = 0.0;

        // update animation frame
        data.rec.x = data.frame * data.rec.width;
        data.frame++;

        if (data.frame > maxFrame)
        {
            data.frame = 0;
        }
    }

    return data;
}

int main()
{
    // Window cordinates
    int win_dimensions[2];
    win_dimensions[0] = 512;
    win_dimensions[1] = 380;

    // Initialize program window
    InitWindow(win_dimensions[0], win_dimensions[1], "Dapper Dasher");

    // Acceleration due to gravity (pixels/sec)/sec
    const int gravity{1000};

    // Textures
    // hazard
    Texture2D nebula = LoadTexture("textures/12_nebula_spritesheet.png");

    // number of hazards
    const int size_of_nebulae{10};

    // anim data for hazard
    AnimData nebulae[size_of_nebulae]{};

    for (int i = 0; i < size_of_nebulae; i++)
    {
        nebulae[i].rec.x = 0.0;
        nebulae[i].rec.y = 0.0;
        nebulae[i].rec.width = nebula.width / 8;
        nebulae[i].rec.height = nebula.width / 8;
        nebulae[i].pos.y = win_dimensions[1] - nebula.height / 8;
        nebulae[i].frame = 0;
        nebulae[i].running_time = 0.0;
        nebulae[i].update_time = 0.0;
        nebulae[i].pos.x = win_dimensions[0] + i * 300;
    }

    float finish_line{nebulae[size_of_nebulae - 1].pos.x};

    // nebula x velocity (pixels/sec)
    int nebula_velocity{-200};

    // anim data for main character
    Texture2D scarfy = LoadTexture("textures/scarfy.png");
    AnimData scarfy_data;
    scarfy_data.rec.width = scarfy.width / 6;
    scarfy_data.rec.height = scarfy.height;
    scarfy_data.rec.x = 0;
    scarfy_data.rec.y = 0;
    scarfy_data.pos.x = win_dimensions[0] / 2 - scarfy_data.rec.width / 2;
    scarfy_data.pos.y = win_dimensions[1] - scarfy_data.rec.height;
    scarfy_data.frame = 0;
    scarfy_data.update_time = 1.0 / 12.0;
    scarfy_data.running_time = 0.0;

    bool is_in_air{};

    // jump velocity (pixels/sec)
    const int jump_velocity{-600};

    // int posY{win_height - rect_height};
    int velocity{0};

    // Background
    Texture2D background = LoadTexture("textures/far-buildings.png");
    Texture2D midground = LoadTexture("textures/back-buildings.png");
    Texture2D foreground = LoadTexture("textures/foreground.png");
    float bg_x{0.0};
    float mg_x{0.0};
    float fg_x{0.0};

    // Set FPS
    SetTargetFPS(60);

    bool collision{};

    while (!WindowShouldClose())
    {
        // delta time (time after last frame)
        float dT{GetFrameTime()};

        // Begin drawing
        BeginDrawing();
        ClearBackground(WHITE);

        bg_x -= 20 * dT;

        if (bg_x <= -background.width * 2)
        {
            bg_x = 0.0;
        }

        mg_x -= 40 * dT;
        if (mg_x <= -background.width * 2)
        {
            mg_x = 0.0;
        }

        fg_x -= 80 * dT;
        if (fg_x <= -background.width * 2)
        {
            fg_x = 0.0;
        }

        // Draw background
        Vector2 bg1_pos{bg_x, 0.0};
        DrawTextureEx(background, bg1_pos, 0.0, 2.0, WHITE);
        // Repeat bg
        Vector2 bg2_pos{bg_x + background.width * 2, 0.0};
        DrawTextureEx(background, bg2_pos, 0.0, 2.0, WHITE);

        Vector2 mg1_pos{mg_x, 0.0};
        DrawTextureEx(midground, mg1_pos, 0.0, 2.0, WHITE);
        Vector2 mg2_pos{mg_x + midground.width * 2, 0.0};
        DrawTextureEx(midground, mg2_pos, 0.0, 2.0, WHITE);

        Vector2 fg1_pos{fg_x, 0.0};
        DrawTextureEx(foreground, fg1_pos, 0.0, 2.0, WHITE);
        Vector2 fg2_pos{fg_x + foreground.width * 2, 0.0};
        DrawTextureEx(foreground, fg2_pos, 0.0, 2.0, WHITE);

        // Ground check
        if (isOnGround(scarfy_data, win_dimensions[1]))
        {
            // rectangle is on the ground
            velocity = 0;
            // prevent air-jump
            is_in_air = false;
        }
        else
        {
            // rectangle is in the air
            // apply gravity
            velocity += gravity * dT;
            is_in_air = true;
        }

        // Character jump
        if (IsKeyPressed(KEY_SPACE) && !is_in_air)
        {
            velocity += jump_velocity;
        }

        // update hazard postion
        for (int i = 0; i < size_of_nebulae; i++)
        {
            nebulae[i].pos.x += nebula_velocity * dT;
        }

        finish_line += nebula_velocity * dT;

        // update main char position
        scarfy_data.pos.y += velocity * dT;

        // update main char animation frame
        if (!is_in_air)
        {
            scarfy_data = updateAnimData(scarfy_data, dT, 5);
        }

        // update hazard animation frame
        for (int i = 0; i < size_of_nebulae; i++)
        {
            nebulae[i] = updateAnimData(nebulae[i], dT, 7);
        }

        for (AnimData nebula : nebulae)
        {
            float pad{50};
            Rectangle neb_rec{
                nebula.pos.x + pad,
                nebula.pos.y + pad,
                nebula.rec.width - 2 * pad,
                nebula.rec.height - 2 * pad};
            Rectangle scarfy_rect{
                scarfy_data.pos.x,
                scarfy_data.pos.y,
                scarfy_data.rec.width,
                scarfy_data.rec.height};
            if (CheckCollisionRecs(neb_rec, scarfy_rect))
            {
                collision = true;
                break;
            }
        }

        if (collision)
        {
            // lose the game
            DrawText("You lose!", win_dimensions[0] / 4, win_dimensions[1] / 2, 40, RED);
        }
        else if (finish_line <= 0)
        {
            // win the game
            DrawText("You win!", win_dimensions[0] / 4, win_dimensions[1] / 2, 40, GREEN);
        }
        else
        {
            // Draw nebula
            for (int i = 0; i < size_of_nebulae; i++)
            {
                DrawTextureRec(nebula, nebulae[i].rec, nebulae[i].pos, WHITE);
            }

            // Draw scarfy
            DrawTextureRec(scarfy, scarfy_data.rec, scarfy_data.pos, WHITE);
        }

        EndDrawing();
    }

    UnloadTexture(nebula);
    UnloadTexture(scarfy);
    UnloadTexture(background);
    UnloadTexture(midground);
    UnloadTexture(foreground);
    CloseWindow();
    // return 0;
}