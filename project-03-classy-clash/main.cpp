#include "raylib.h"

int main()
{
    // Window cordinates
    int win_dimensions{384};
    // Initialize window
    InitWindow(win_dimensions, win_dimensions, "Classy Clash");

    // Background
    Texture2D background = LoadTexture("nature_tileset/world-map.png");

    SetTargetFPS(60);

    while (!WindowShouldClose())
    {
        // ---- Game updates ----

        // Begin drawing
        BeginDrawing();
        ClearBackground(WHITE);

        // Draw background
        Vector2 map_pos{0.0, 0.0};
        DrawTextureEx(background, map_pos, 0.0, 4.0, WHITE);

        // ---- Game drawings ----

        EndDrawing();
    }

    UnloadTexture(background);

    CloseWindow();
}
