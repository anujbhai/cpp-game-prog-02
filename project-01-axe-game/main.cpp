#include "raylib.h"

int main()
{
    // Window cordinates
    int width{800};
    int height{450};

    // Circle cordinates
    int circle_x{200};
    int circle_y{200};
    int circle_radius{25};
    // Circle edges
    int lt_circle_x{circle_x - circle_radius};
    int rt_circle_x{circle_x + circle_radius};
    int upper_circle_y{circle_y - circle_radius};
    int bottom_circle_y{circle_y + circle_radius};

    // Axe cordinates
    int axe_x{width / 2};
    int axe_y{0};
    int axe_dim{50};
    // Axe edges
    int lt_axe_x{axe_x};
    int rt_axe_x{axe_x + axe_dim};
    int upper_axe_y{axe_y};
    int bottom_axe_y{axe_y + axe_dim};

    // Directions
    int direction{10};

    // Collision detection
    bool collision_with_axe =
        (bottom_axe_y >= upper_circle_y) &&
        (upper_axe_y <= bottom_circle_y) &&
        (rt_axe_x >= lt_circle_x) &&
        (lt_axe_x <= rt_circle_x);

    // Initialize program window
    InitWindow(width, height, "Anuj's Window.");

    // Set fps
    SetTargetFPS(60);

    while (WindowShouldClose() != true)
    {
        BeginDrawing();
        ClearBackground(WHITE);

        // Check collision
        if (collision_with_axe)
        {
            DrawText("Game Over!!", 400, 200, 20, RED);
        }
        else
        {
            // Update object edges
            lt_circle_x = circle_x - circle_radius;
            rt_circle_x = circle_x + circle_radius;
            upper_circle_y = circle_y - circle_radius;
            bottom_circle_y = circle_y + circle_radius;

            lt_axe_x = axe_x;
            rt_axe_x = axe_x + axe_dim;
            upper_axe_y = axe_y;
            bottom_axe_y = axe_y + axe_dim;

            // Update collision with axe
            collision_with_axe =
                (bottom_axe_y >= upper_circle_y) &&
                (upper_axe_y <= bottom_circle_y) &&
                (rt_axe_x >= lt_circle_x) &&
                (lt_axe_x <= rt_circle_x);

            // Game logic starts
            DrawCircle(circle_x, circle_y, circle_radius, BLUE);
            DrawRectangle(axe_x, axe_y, axe_dim, axe_dim, RED);

            // Circle movement
            if (IsKeyDown(KEY_D) && circle_x < width)
            {
                circle_x += 10;
            }
            else if (IsKeyDown(KEY_A) && circle_x > 0)
            {
                circle_x -= 10;
            }

            // Move axe
            axe_y += direction;

            if (axe_y > height || axe_y < 0)
            {
                direction = -direction;
            }
            // Game logic ends
        }

        EndDrawing();
    }
}